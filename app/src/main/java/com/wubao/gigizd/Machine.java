package com.wubao.gigizd;

import com.invertor.modbus.Modbus;
import com.invertor.modbus.ModbusMaster;
import com.invertor.modbus.ModbusMasterFactory;
import com.invertor.modbus.exception.ModbusIOException;
import com.invertor.modbus.exception.ModbusNumberException;
import com.invertor.modbus.exception.ModbusProtocolException;
import com.invertor.modbus.serial.SerialParameters;
import com.invertor.modbus.serial.SerialPort;
import com.invertor.modbus.serial.SerialPortFactoryJSSC;
import com.invertor.modbus.serial.SerialUtils;

import org.jetbrains.annotations.NotNull;

/**
 * @author jokerliang
 */

public class Machine {
    private ModbusMaster master;

    public void connect(String port) throws Exception {

        SerialParameters sp = new SerialParameters();
        Modbus.setLogLevel(Modbus.LogLevel.LEVEL_DEBUG);

        sp.setDevice(port);

        sp.setBaudRate(SerialPort.BaudRate.BAUD_RATE_38400);
        sp.setDataBits(8);
        sp.setParity(SerialPort.Parity.NONE);
        sp.setStopBits(1);

        SerialUtils.setSerialPortFactory(new SerialPortFactoryJSSC());

        if (master != null) {
            master.disconnect();
        }

        master = ModbusMasterFactory.createModbusMasterRTU(sp);
        master.connect();
    }

    public void disconnect() throws Exception {
        if (master != null) {
            master.disconnect();
        }
    }



    public int getValue(int startAddress) {

        if (master == null) {
            return 0;
        }
        int[] values = new int[1];
        try {
            values = master.readHoldingRegisters(2, startAddress, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return values[0];
    }

    protected void setValue(int startAddress, int value) {

        try {
            master.writeSingleRegister(2, startAddress, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    public String getPad (int len, int n) {
        return String.format("%" + String.valueOf(len) + "s", Integer.toBinaryString(n)).replace(' ', '0');
    }

    public String reverse (String str) {
        return new StringBuilder(str).reverse().toString();
    }

    @NotNull
    public String getMachineStatus() {
        int value = getValue(0x03);
        return getPad(16, value);
    }

    public void shipment(String layer, String column) {
        String address = layer + column;
        setValue(0x05, Integer.parseInt(address));
    }

}
