package com.wubao.gigizd

import android.util.Log

/**
求贤若饥 虚心若愚
 * @author jokerliang
 * @date 2020-02-21 11:20
 */
fun Any.addLog(logStr: String, tag: String = "joker") {
    Log.d(tag, logStr)
}