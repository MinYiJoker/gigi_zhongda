package com.wubao.gigizd

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.log

class MainActivity : AppCompatActivity() {

    val machine by lazy { Machine() }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initSdk()
        initClick()

        startShipment()

        startTemp()
    }

    private fun initSdk() {

    }

    private fun startTemp() {
        queryTemp.setOnClickListener {

        }

    }

    private fun startShipment() {
        shipment.setOnClickListener {
            val layer = layerInput.text.toString().trim()
            val column = columnInput.text.toString().trim()
            if (layer == "" || column == "") {
                addLog("行或列不允许为空", isImportance = true)
                return@setOnClickListener
            }
            addImportanceLog("开始发送出货指令。行：$layer  -- 列：$column")


            // 这里看上去 是要转16进制
            // 现在是10进制
            machine.shipment(layer, column)
        }


        shipment1.setOnClickListener {
            val address = address.text.toString().trim()
            if (address == "") {
                addLog("行或列不允许为空", isImportance = true)
                return@setOnClickListener
            }
            addImportanceLog("开始发送出货指令。行列：$address")

        }

    }

    private fun initClick() {
        clearLog.setOnClickListener {
            logRoot.removeAllViews()
        }
        connectBtn.setOnClickListener {
            addLog("连接机器")
            try {
                machine.connect("/dev/ttyS1")
                addLog("连接机器成功, 开始轮训机器状态")
                Thread{
                    while (true) {
                        Thread.sleep(2000)
                        val machineStatus = machine.getMachineStatus()


                        runOnUiThread {
                            addLog(machineStatus)
                        }
                    }
                }.start()
            } catch (e: Exception) {
                addLog("连接机器失败错误： ${e.message}", isImportance = true)
            }
        }

        openShipmentReturn.setOnClickListener {
            addLog("开启调货检测")
            try {

            } catch (e: Exception) {
                addImportanceLog("调用出现异常")
                if (e.message != null) {
                    addImportanceLog(e.message!!)
                }

            }
        }

        closeShipmentReturn.setOnClickListener {
            addLog("关闭调货检测")

            try {

            } catch (e: Exception) {
                addImportanceLog("调用出现异常")
                if (e.message != null) {
                    addImportanceLog(e.message!!)
                }

            }
        }

        setModeBtn.setOnClickListener {
            val trim = modeInput.text.toString().trim()

        }

        modeOne.setOnClickListener {

        }

        modeTWO.setOnClickListener {

        }
    }


    private val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

    private fun addImportanceLog(logStr: String) {
        addLog(logStr, isImportance = true)
    }

    private fun addLog(logStr: String, isImportance: Boolean = false, tag: String = "joker") {
        runOnUiThread {
            Log.d(tag, logStr)
            val textView = TextView(this)
            val time = simpleDateFormat.format(Date())
            textView.text = "$time:  $logStr"
            textView.textSize = 30f
            if (isImportance) {
                textView.setTextColor(Color.RED)
            } else {
                textView.setTextColor(Color.BLACK)
            }
            logRoot.addView(textView)

        }

    }


    override fun onDestroy() {

        super.onDestroy()
    }



}
