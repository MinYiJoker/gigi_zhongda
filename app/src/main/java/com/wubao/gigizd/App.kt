package com.wubao.gigizd

import android.app.Application
import com.tencent.bugly.crashreport.CrashReport

/**
求贤若饥 虚心若愚
 * @author jokerliang
 * @date 2020-02-24 19:48
 */
class App: Application() {
    override fun onCreate() {
        super.onCreate()
        initBugLy()
    }

    private fun initBugLy() {
        val strategy = CrashReport.UserStrategy(applicationContext)

        CrashReport.setIsDevelopmentDevice(applicationContext, BuildConfig.DEBUG)
        CrashReport.initCrashReport(applicationContext, "227246ef54", BuildConfig.DEBUG, strategy)
    }
}